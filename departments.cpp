#include <iostream>
#include <string>

using namespace std;

class Dziekanat {
public:
    string pracownik_imie, opis, dziekanat, wydzial;
    int liczbastudentow, nr;
    static int licznik;
//konstuktor
    Dziekanat(string _dziekanat="ETI", string _wydzial="IMIIP",
              string _pracownik_imie="Andrzej", string _opis="dziakanat eti",
              int _liczbastudentow=0):
        dziekanat(_dziekanat),
        wydzial(_wydzial),
        pracownik_imie(_pracownik_imie),
        opis(_opis),
        liczbastudentow(_liczbastudentow)
        {nr = ++licznik;}

    double operator+(Dziekanat D){ //przeciazanie operadora dodawania
        return liczbastudentow + D.liczbastudentow;}
    double operator-(Dziekanat D){ //odejmowania
        return liczbastudentow - D.liczbastudentow;}
    double operator*(Dziekanat D){ //mnozenia
        return liczbastudentow * D.liczbastudentow;}
    Dziekanat& operator<<(int przesuniecie){ //przesuniecie bitowe
        cout << "Przesuwam bitowo liczbe studentow: " << liczbastudentow
             << ", o jedno miejce bitowe w lewo" << endl;
        liczbastudentow = liczbastudentow << przesuniecie;
        return *this;
    }

	~Dziekanat() {} //destruktor
};
//przeciazanie operatora wyswietlania - wyswietlenie obiekow
ostream& operator<<(ostream& out, Dziekanat& dziek){
    out << "\n\t----- Dziekanat numer: "<< dziek.nr
        << " -----\n Opis: "            << dziek.opis
        << "\n Liczba studentow: "      << dziek.liczbastudentow
        << "\n Nazwa: "                 << dziek.dziekanat
        << "\n Wydzial: "               << dziek.wydzial
        << "\n";
    return out;
}
//deklaracja pola statycznego
int Dziekanat::licznik = 0;
//klasa dziedziczaca
class Pracownik : public Dziekanat{
public:
    string *nazwisko;
    Pracownik(string imie = "NoweImie", string nazwisko = "NoweNazwisko");
    Pracownik& operator=(Pracownik& pr){ //przeciazenie operatora przypisania
        delete nazwisko;
        nazwisko = new string;
        pracownik_imie = pr.pracownik_imie;
        *nazwisko = *nazwisko;
    }
};
Pracownik::Pracownik(string _imie, string _nazwisko){
    pracownik_imie = _imie;
    nazwisko = new string;
    *nazwisko = _nazwisko;
}
//wyswietlnie obiektow
ostream& operator<<(ostream& out, Pracownik& prac){
    out << "\n Imie: "      << prac.pracownik_imie
        << "\n Nazwisko: "  << *prac.nazwisko
        << "\n";
    return out;
}

int main() {
    //tworzenie dziekanatow
	Dziekanat Dziekanat1("Wiertnictwo", "Odlewnik",
                         "Marek", "sprawy socjalne", 10);
	Dziekanat Dziekanat2("IS", "IMIiP",
                         "Bartosz", "informacja studencka", 4);
    Dziekanat Dziekanat3;
    //wyswietlenie dziekanatow
    operator<<(cout,"\t\t ------ Zestawienie wydzialow ------ \n");
    operator<<(cout, Dziekanat1); //jawne
    cout << Dziekanat2 << Dziekanat3; //niejawne
    //tworzenie i wyswietlenie pracownikow
    cout << "\nPracownicy:";
    Pracownik Pracownik1("Adam", "Nowak");
    Pracownik Pracownik2;
    cout << Pracownik1 << Pracownik2;
    //rozne operacje przciazania na obiekatch
    cout << "\n\tOperacje na obiekatach\n\n";

    cout << "--Operator przypisania: ";
    Pracownik2 = Pracownik1;
    *Pracownik2.nazwisko = "Gorgol";
    cout << Pracownik1 << Pracownik2 << endl;

    cout << "--Operator dodawnia: \n";
    cout << "Dziekanat1+Dziekanat2: " << Dziekanat1+Dziekanat2;

    cout << "\n\n--Operator odejmowania: \n";
    cout << "Dziekanat1-Dziekanat2: " << Dziekanat1-Dziekanat2;

    cout << "\n\n--Operator mnozenia: \n";
    cout << "Dziekanat1*Dziekanat2: " << Dziekanat1*Dziekanat2;

    cout << "\n\n--Przesuniecie bitowe: \n";
    Dziekanat2 = Dziekanat2 << 2;
    cout << "Liczba studentow wynosi: ";
    cout.operator<<(Dziekanat2.liczbastudentow);

    cout<<endl;
    return 0;

}

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class kolo {
    double pole;
    float promien;
    int wypelnienie;
    static int nrKol;
    int nr;
    kolo() {pole=0;nr = ++nrKol;} //konst bezparam
    kolo(float p,int w) {
        promien = p;
        wypelnienie = w;
        pole = 0;
        nr = ++nrKol;
    }

public:
    Oblicz_Pole(){ pole = M_PI * promien * promien; }
    Ustaw_Promien(float pr){
        this->promien = pr;
        cout<<"Zmiana promienia kola nr "<<nr<<" na "<<promien<<endl;
    }
    Ustaw_Kolor(int wp){ this->wypelnienie = wp; }
    static kolo* zrob_kolo () {return new kolo();} //metoda do obslugi konst prywatnego
    static kolo* zrob_kolo2 (float p,int w) {return new kolo(p,w);} //metoda do obslugi konst prywatnego
    char koloinfo () {
        cout<<"--Kolo numer "<<nr<<"--"<<endl
            <<"promien: "<<promien<<", "
            <<"pole: "<<pole<<", "
            <<"wypelnienie: "<<wypelnienie<<endl;
    }
};

int kolo::nrKol = 0;

int main() {
    cout<<"--- Tworzenie dwoch obiektow: ---"<<endl;
    cout<<"wartosci domyslne: "<<endl;
    kolo* K1 = kolo::zrob_kolo(); //tworzenie obj z konst bezparam
    K1->koloinfo();

    kolo* K2 = kolo::zrob_kolo2(5.,334455); //tworzenie obj z konst param
    K2->koloinfo();
    cout<<endl;

    cout<<"--- Ustawienie tego samego koloru: ---"<<endl;
    K1->Ustaw_Kolor(441155);
    K2->Ustaw_Kolor(441155);
    K1->koloinfo();
    K2->koloinfo();
    cout<<endl;

    cout<<"--- Ustawienie promienia: ---"<<endl;
    K1->Ustaw_Promien(7.);
    K2->Ustaw_Promien(16.);
    K1->koloinfo();
    K2->koloinfo();
    cout<<endl;

    cout<<"--- Obliczenie pola powierzchni kola: ---"<<endl;
    K1->Oblicz_Pole();
    K2->Oblicz_Pole();
    K1->koloinfo();
    K2->koloinfo();
    cout<<endl;

    //niszczenie obiekow
    delete K1;
    delete K2;
    //system ("Pause");
    return 0;
}


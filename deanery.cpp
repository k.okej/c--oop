#include <iostream>
#include <string>

using namespace std;

class Dziekanat {
public:
	class DziekanatData { //klasa zagniezdzona
	public:
		string pracownik, fullinfo;
		int licz = 0;
	};

	Dziekanat(); //konstruktor bezparametrowy

	Dziekanat(string dziekanat, string wydzial, string pracownik, string opis); // konstruktor z paramaetrami

	Dziekanat(const Dziekanat& DziekanatCopy) { // konstruktor kopiujacy
		dziekanat = DziekanatCopy.dziekanat;
        wydzial = DziekanatCopy.wydzial;
        opis.pracownik = DziekanatCopy.opis.pracownik;
        opis.fullinfo = DziekanatCopy.opis.fullinfo;
        opis.licz++;
    };

	~Dziekanat();

private:
	string dziekanat, wydzial;
	DziekanatData opis;

public:
	string czytaj_dane() { //umo�liwia wprowadzenie do programu warto�ci z klawiatury
		cout << "podaj nazwe kierunku: ";
		cin >> dziekanat;
		cout << "podaj wydzial: ";
		cin >> wydzial;
		return dziekanat, wydzial;
	};
	void licznik () { //licznik utworzonych obiektow classy dzziekanat
        opis.licz++;
    };
	void wyswietl_wynik() { // wy�wietla warto�ci
	    cout << dziekanat << " " << wydzial << " ID:" << opis.licz << endl;
    };
	void wyswietl_wszystkie() { // wy�wietla wszystkie warto�ci
		wyswietl_wynik();
		cout << opis.pracownik << " " << opis.fullinfo << endl;
	};
};


Dziekanat::Dziekanat() {}
Dziekanat::Dziekanat(string _dziekanat, string _wydzial, string _pracownik, string _opis) {
	dziekanat = _dziekanat;
	wydzial = _wydzial;
	opis.pracownik = _pracownik;
	opis.fullinfo = _opis;
	Dziekanat::licznik(); //zwiekszenie licznika o 1
}
Dziekanat::~Dziekanat() { //destruktor
	cout << "Niszczenie obiektu" << endl;
	wyswietl_wynik();
	cout << " pozostalo ID:";
    opis.licz--;
    cout << opis.licz <<endl<<endl;
}


int main() {

	Dziekanat Dziekanat1("Wiertnictwo", "Odlewnik", "Marek", "sprawy socjalne");
    Dziekanat1.wyswietl_wynik();
	Dziekanat1.wyswietl_wszystkie();
	cout<<endl;

	Dziekanat Dziekanat2("IS", "IMIiP", "Bartosz", "informacja studencka");
	Dziekanat2.czytaj_dane();
	Dziekanat2.wyswietl_wynik();
	cout<<endl;

    Dziekanat* Dziekanat3 = new Dziekanat("ETI", "IMIiP", "Helena", "sprawy dydaktyczne"); //deklaracja dynamiczna
	Dziekanat3->wyswietl_wszystkie();
    cout<<endl;

	cout<<endl;
	delete Dziekanat3;


}
